%Define our movie and scale factor
%cd demo_people;
addpath(genpath('.'));
frs = 1:200;

dirBgs = '/home/sandeeps/git/demo-people/ims/group31/bgs_new/';
dirOrig = '/home/sandeeps/git/demo-people/ims/group31/';
movieS = [dirBgs,'groupscope_%.8d.png'];
outDir = [dirOrig,'out_new_test/'];
movieOrig = [dirOrig,'groupscope_%.8d.png'];
movieOut = [outDir,'groupscope_%.8d.png'];
mkdir(outDir);

%movieS = 'ims/baseball/%.8d.jpg';
%movieS = 'ims/crowd/image_%.8d_0.png';
%movieS = 'ims/pedestrians/image_%.8d_1.png';
%movieS = 'groupscope_%.8d.png';
%movieS = 'ims/out/%.1d.png';
imscale = 0.8;

%First step; run stylized pose detector on each frame
INIT_DETECT = 0;
if INIT_DETECT
    %First, run our stylized person detector
    segsAll = cell(length(frs),1);
    parfor fr = frs,
    %parfor fr = 260
	disp(['frame = ', num2str(fr)]);	
        %Read in image, scaling it so torso is roughly 50 pixels long
        imOrig = imread(sprintf(movieS,fr));
        %For this sequence, hard-code in we're looking for some-one walking to the right with a torso 50 pixels high
        im = imresize(imOrig(:,end:-1:1,:),imscale,'bilinear');
        % Because the walking detector involves sampling, may have to do this multiple times
        % and take best-scoring one
        segs = findWalkingPerson(im);        
        %Flip and re-size to regular image
        if isfield(segs,'x') && isfield(segs,'u') && isfield(segs,'y') && isfield(segs,'w'),
            [segs.x,segs.u] = deal(size(im,2) - segs.x + 1,-segs.u);
            [segs.x,segs.y,segs.len,segs.w] = deal(segs.x/imscale,segs.y/imscale,segs.len/imscale,segs.w/imscale);
            %Build an appearance model for each limbs and evaluate how good we are
            modelSegs = buildLimbModelLin({imOrig},{segs});
            %Sum up the fraction of missclassified pixels, downweighting the upper arm by .5
            segs.cost = [1 .5 1 1 1 1 1 1] * modelSegs.err;
            segsAll{fr} = segs;
            disp(['frame = ', num2str(fr), ' segs.cost = ', num2str(segs.cost)]);
        end
	disp(['frame = ', num2str(fr), ' end']);
    end

    %Take the best scoring one
    costs = repmat(100,length(frs),1);
    parfor i = 1:length(frs),
        if ~isempty(segsAll{i}),
            costs(i) = segsAll{i}.cost;
        end
    end       
    
    [dummy,fr] = min(costs);
    disp(['min cost = ', dummy]);
    %[sortedCosts,frameIndices] = sort(costs);
    %fr = frameIndices(2);
    
    %disp(['min=', sortedCosts(2)]);
    
    %Build a quadratic logistic regression model for each limb
    im = imread(sprintf(movieS,fr));
    modelSegs = buildLimbModelQuad({im},segsAll(fr));
    
    save walkingDetections segsAll modelSegs;
else
    load walkingDetections;
end

%Track by detecting with learned appearance model
trackSegs = cell(length(frs),1);
parfor fr = frs,
  fr
  clf;
  im = imread(sprintf(movieS,fr));
  imTest = imread(sprintf(movieOrig,fr));
  %Can search over image and different scales if desired
  im = imresize(im,imscale,'bilinear');
  imTest = imresize(imTest,imscale,'bilinear');
  
  %Start: Modified to run on Groupscope server    
  %Sample body poses by computing the posterior with the sum-product algorithm
  [pts,cost] = findGeneralPerson(im,modelSegs);  
  %Find the modes in the samples
  trackSegs{fr} = findModePose(pts);
  [r,c,d] = size(im);
  set(gca,'Units','normalized','Position',[0 0 1 1]);  %# Modify axes size
  set(gcf,'Units','pixels','Position',[200 200 c r]);  %# Modify figure size  
  imTest = showsegIm(imTest,trackSegs{fr});  
  title('Mode in posteri testing');

  %frame = getframe(gcf);
  imwrite(imTest, sprintf(movieOut,fr),'png');
  %End: Modified to run on Groupscope server  
  
  %%%Old code commented out%%%
%   subplot(231);
%   imshow(im); title(sprintf('Frame %d',fr));
%   %Sample body poses by computing the posterior with the sum-product algorithm
%   [pts,cost] = findGeneralPerson(im,modelSegs);
%   subplot(235);
%   showPersonPts(size(im),pts);
%   title('Posterior');  
%   
%   %Find the modes in the samples
%   trackSegs{fr} = findModePose(pts);  
%   showsegIm(im,trackSegs{fr});     
%   title('Mode in posterior'); 
  
  drawnow;
end

%Show the track
clf;
%set(gcf,'doublebuffer','on');
%for fr = frs,
%  im = imread(sprintf(movieS,fr));
%  %Can search over image and different scales if desired
%  im = imresize(im,imscale);
%  showsegIm(im,trackSegs{fr});
%  drawnow;
%end
