function outSegs = evaluatePersonDisc(imsAll,segsAll)
% segs = evaluatePerson(im,segs)

NTYPE = 10; TOR = 1;
LUA = 2; RUA = 3; LUL = 4; RUL = 5;
LLA = 6; RLA = 7; LLL = 8; RLL = 9;
HED = 10;
NN = length(segsAll);
BUFFER = 30;
test = 10;
[sizy,sizx,sizz] = size(imsAll{1});
[sizy,sizx] = deal(sizy - BUFFER*2,sizx - BUFFER*2);
legMask = cell(NN,1);
armMask = cell(NN,1);

for i = 1:NN,
  if ~any(segsAll{i}.type == HED),
    %Add a head
    head = subSegs(segsAll{i},segsAll{i}.type == TOR);
    hWid = head.w;
    [head.y head.len head.w] = deal(head.y-head.len-hWid,hWid,hWid);
    head.type = HED;
    segsAll{i} = appendSegs(segsAll{i},head);
    %segsAll{i} = addHead(imsAll{i},segsAll{i});
  end
  [segsAll{i}.x,segsAll{i}.y] = deal(segsAll{i}.x - BUFFER,segsAll{i}.y - BUFFER);
  legMask{i} = find(showModelIm([sizy sizx sizz],subSegs(segsAll{i},ismember(segsAll{i}.type,[LUL RUL LLL RLL]))));
  armMask{i} = find(showModelIm([sizy sizx sizz],subSegs(segsAll{i},ismember(segsAll{i}.type,[LUA LLA]))));
  %Ignore border effects
  imsAll{i} = imsAll{i}(BUFFER+1:end-BUFFER,BUFFER+1:end-BUFFER,:);
  imsAll{i} = reshape(imsAll{i},sizy*sizx,sizz);
end

outSegs = segsAll{1};
DD = sizz+1;
outSegs.b = zeros(length(outSegs.type),DD);
[outSegs.lli,outSegs.err] = deal(zeros(size(outSegs.type)));

for i = [TOR LUA LLA LUL LLL HED],
  %i
  if ismember(i,[LUL LLL]) & sum(ismember(outSegs.type,[i i+1])) == 2,
    segTypes = [i i+1];
  else
    segTypes = i;
  end
  fgPixels = [];
  bgPixels = [];
  for fr = 1:NN,
    segs = segsAll{fr};
    %keyboard;
    for segType = segTypes,
      pseg = subSegs(segs,segs.type == segType);
      mask = showModelIm([sizy sizx sizz],pseg);
      if segType == TOR,
	%Remove any occluding arms
	mask(armMask{fr}) = 0;
      end
      fgPixels = [fgPixels; double(imsAll{fr}(find(mask),:))];
      
      %Define a subset of the background pixels to evaluate
      %(the 2 rectangles flanking our orignal rectangle)
      xys = [pseg.x + pseg.v*pseg.w*2, pseg.x - pseg.v*pseg.w*2;
	     pseg.y - pseg.u*pseg.w*2, pseg.y + pseg.u*pseg.w*2];
      for xy = xys,
	[pseg.x,pseg.y] = deal(xy(1),xy(2));
	mask = showModelIm([sizy sizx sizz],pseg);
	%Throw away body pixels in the background, if we're dealing with
	%a symmetric segment
	if length(segTypes) > 1,
	  mask(legMask{fr}) = 0;
	end
	bgPixels = [bgPixels; double(imsAll{fr}(find(mask),:))];	
      end
    end    
  end
  %fgPixels = dat2polyc(fgPixels);
  %bgPixels = dat2polyc(bgPixels);
  
  %Learn the model for the entire image
  %np = size(fgPixels,1); nn = size(bgAllPixels,1);
  %y = [ones(np,1); zeros(nn,1)];
  %x = [[fgPixels;bgAllPixels] ones(size(y))];
  %w = [ones(np,1)/np; 5*ones(nn,1)/nn]; 
  
  np = size(fgPixels,1); nn = size(bgPixels,1);
  y = [ones(np,1); zeros(nn,1)];
  x = [[fgPixels;bgPixels] ones(size(y))];
  w = [ones(np,1)/np; 2*ones(nn,1)/nn];        
  [b,p,lli] = logist2(y,x,w);
  outSegs.b(ismember(segs.type,segTypes),:) = repmat(b',length(segTypes),1);
  
  %Try the model on the immediate neighborhood
  np = size(fgPixels,1); nn = size(bgPixels,1);
  y = [ones(np,1); zeros(nn,1)];
  x = [[fgPixels;bgPixels] ones(size(y))];
  w = [ones(np,1)/np; 2*ones(nn,1)/nn];    
  post = 1./(1 + exp(-x*b));
  outSegs.lli(ismember(segs.type,segTypes)) = -(w' * (y.*log(post+eps) + (1-y).*log(1-post+eps)));
  outSegs.err(ismember(segs.type,segTypes)) = w'* (y ~= (post > .5));
end

%Fix indexing
[outSegs.x,outSegs.y] = deal(outSegs.x + BUFFER,outSegs.y + BUFFER);

%segType = LLA;
%post = reshape(1./(1+ exp(-[reshape(double(im),[sizy*sizx,size(im,3)]) ones(sizy*sizx,1)]*segs.b(segs.type == segType,:)')),sizy,sizx);
%imagesc(post > .5);





