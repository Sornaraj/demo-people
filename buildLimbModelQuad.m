function outSegs = evaluatePersonDisc2(imsAll,segsAll)
% segs = evaluatePersonDisc2(imsAll,segsAll)
% Fits a discriminitive model to the segsAll
% Uses the rest of body as negative pixels as well

NTYPE = 10; TOR = 1;
LUA = 2; RUA = 3; LUL = 4; RUL = 5;
LLA = 6; RLA = 7; LLL = 8; RLL = 9;
HED = 10;

NN = length(segsAll);
BUFFER = 10;
[sizy,sizx,sizz] = size(imsAll{1});
[sizy,sizx] = deal(sizy - BUFFER*2,sizx - BUFFER*2);



for i = 1:NN,
  if ~any(segsAll{i}.type == HED),
    %Add a head
    head = subSegs(segsAll{i},segsAll{i}.type == TOR);
    hWid = head.w;
    [head.y head.len head.w] = deal(head.y-head.len-hWid,hWid,hWid);
    head.type = HED;
    segsAll{i} = appendSegs(segsAll{i},head);
  end
  [segsAll{i}.x,segsAll{i}.y] = deal(segsAll{i}.x - BUFFER,segsAll{i}.y - BUFFER);
  legMask{i} = find(showModelIm([sizy sizx sizz],subSegs(segsAll{i},ismember(segsAll{i}.type,[LUL RUL LLL RLL]))));
  armMask{i} = find(showModelIm([sizy sizx sizz],subSegs(segsAll{i},ismember(segsAll{i}.type,[LUA LLA]))));
  %Ignore border effects
  imsAll{i} = imsAll{i}(BUFFER+1:end-BUFFER,BUFFER+1:end-BUFFER,:);
  imsAll{i} = reshape(imsAll{i},sizy*sizx,sizz);
end

outSegs = segsAll{1};
%outSegs.type = segsAll{1}.type;
DD = 2*sizz + nchoosek(sizz,2) + 1;
outSegs.b = zeros(length(outSegs.type),DD);
[outSegs.lli,outSegs.err] = deal(zeros(size(outSegs.type)));


for i = [TOR LUA LLA LUL LLL HED],
  i
  if ismember(i,[LUL LLL]),
    segTypes = [i i+1];
  else
    segTypes = i;
  end
  fgPixels = [];
  bgPixels = [];
  
  for fr = 1:NN,
    segs = segsAll{fr};
    for segType = segTypes,
      if segType == LLA,
	%keyboard;
      end
      pseg = subSegs(segs,segs.type == segType);
      mask = showModelIm([sizy sizx sizz],pseg);
      if segType == TOR,
	%Remove any occluding arms
	mask(armMask{fr}) = 0;
      end
      fgPixels = [fgPixels; double(imsAll{fr}(find(mask),:))];
      
      %Switch to the background
      mask = ~mask;
      %Throw away body pixels in the background, if we're dealing with
      %a symmetric segment
      if length(segTypes) > 1,
	mask(legMask{fr}) = 0;
      end
      tmp = imsAll{fr}(find(mask),:);
      if NN > 1,
	tmp = tmp(randset(round(size(tmp,1)/NN),size(tmp,1)),:);
      end
      bgPixels = [bgPixels; double(tmp)];      
    end
  end
  fgPixels = dat2polyc(fgPixels);
  bgPixels = dat2polyc(bgPixels);
  
  np = size(fgPixels,1); nn = size(bgPixels,1);
  y = [ones(np,1); zeros(nn,1)];
  x = [[fgPixels;bgPixels] ones(size(y))];
  w = [ones(np,1)/np; 5*ones(nn,1)/nn];        
  [b,p,lli] = logist2(y,x,w);
  outSegs.b(ismember(outSegs.type,segTypes),:) = repmat(b',sum(ismember(outSegs.type,segTypes)),1);
  
  %Try the model on the immediate neighborhood
  np = size(fgPixels,1); nn = size(bgPixels,1);
  y = [ones(np,1); zeros(nn,1)];
  x = [[fgPixels;bgPixels] ones(size(y))];
  w = [ones(np,1)/np; 5*ones(nn,1)/nn];    
  post = 1./(1 + exp(-x*b));
  outSegs.lli(outSegs.type == segType) = -(w' * (y.*log(post+eps) + (1-y).*log(1-post+eps)));
  outSegs.err(outSegs.type == segType) = w'* (y ~= (post > .5));    
end
[outSegs.x,outSegs.y] = deal(outSegs.x + BUFFER,outSegs.y + BUFFER);


%segType = 1;
%fr = 1;
%[sizy sizx sizz] = size(imsAll{fr});
%post = reshape(1./(1+ exp(-[dat2poly(reshape(double(imsAll{fr}),[sizy*sizx,size(imsAll{fr},3)])) ones(sizy*sizx,1)]*outSegs.b(outSegs.type == segType,:)')),sizy,sizx);
%imagesc(post);









