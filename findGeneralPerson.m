function [pts,cost] = findGeneralPerson(im,modelSegs)
%[pts,cost] = findGeneralPerson(imOrig,modelSegs)

VERBOSE = 0;


%Define canonical scale of a person
NTYPE = 10; TOR = 1;
LUA = 2; RUA = 3; LUL = 4; RUL = 5;
LLA = 6; RLA = 7; LLL = 8; RLL = 9;
HED = 10;

%Body porportions
S.lens = [45 30 30 40 40 30 30 40 40 15];
S.wids = [25 7 7 13 13 7 7 13 13 15];

MAX_NUM = 2000; %Number of candidate body parts
BIG_NUM = 1e10;

%Store missed detection threholds
%If the reponse of a body part is lower than these, we'll assume it's occluded
P_MD = exp(-5)*ones(NTYPE,1);
P_MD([LLA RLA LLL RLL]) = exp(-2.5);


%For speed up, we'll shrink images when searching for limbs
%(NOTE: Just for speed up; can always set these to 1)
fracs = ones(size(S.lens));
fracs(TOR) = .5;
fracs([LUL LLL]) = .3;
fracs(HED) = .5;

%Do low-level search for segments
[imy,imx,imz] = size(im);
imBig = [dat2polyc(reshape(im,imy*imx,imz)) ones(imy*imx,1)];
segs = [];
for segType = [TOR LUA LLA LUL LLL HED],
    betaBig = modelSegs.b(modelSegs.type == segType,:);
    len = round(S.lens(segType)*fracs(segType));
    wid = round(S.wids(segType)*fracs(segType));
    costIm = reshape(imBig*betaBig' > 0,imy,imx);
    if VERBOSE,
        switch segType
            case TOR
                subplot(232); imshow(costIm); title('Torso pixels');
            case LLA
                subplot(233); imshow(costIm); title('Lower arm pixels');
            case LLL
                subplot(234); imshow(costIm); title('Lower leg pixels');
        end
    end
    costIm = imresize(costIm,fracs(segType));      
    if segType == TOR,
      res = searchImCostTRandc(costIm,len,wid,MAX_NUM,P_MD(segType));
    elseif segType == HED,
	  res = searchImCostCircleRandc(costIm,wid,MAX_NUM,P_MD(segType));      
    else
	  res = searchImCostTRandc(costIm,len,wid,MAX_NUM,P_MD(segType));
    end
    [res.x,res.y,res.len(:),res.w(:)] = deal(res.x/fracs(segType),res.y/fracs(segType),S.lens(segType)/2,S.wids(segType)/2);
    res.type = segType*ones(size(res.resp));
    segs = appendSegs(segs,res);
end
%If no segments were found, set them to 0
segs.resp = max(segs.resp,eps);
segs.resp = -log(segs.resp);
%Downweight the upper arms and legs
segs.resp(ismember(segs.type,[LUA LUL])) = segs.resp(ismember(segs.type,[LUA LUL]))*.75;
[pts,cost] = sample_torso_arm_legc(subSegs(segs,segs.type == TOR),subSegs(segs,segs.type == HED),subSegs(segs,segs.type == LUA),subSegs(segs,segs.type == LLA),subSegs(segs,segs.type == LUL),subSegs(segs,segs.type == LLL));
%showPersonPts(size(im),pts);
