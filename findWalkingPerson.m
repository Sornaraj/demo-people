function segs = findWalkingPerson(im)
%segs = findWalkingPerson(im)

%Define canonical scale of a person
NTYPE = 10; TOR = 1;
LUA = 2; RUA = 3; LUL = 4; RUL = 5;
LLA = 6; RLA = 7; LLL = 8; RLL = 9;
HED = 10;

S.lens = [55 30 40 40 40 40 40 40 40 15];
S.wids = [17 7 7 13 13 7 7 13 13 15];

MAX_NUM = 3000; %Number of candidate body parts
BIG_NUM = 1e10;

load torso_templates;
hWid = (size(torso_templates,1) - S.lens(1))/2;
hWid = round(hWid);

segs = [];

%Get edges
[edgeIm,t] = mydetGMc(im,2,24);
edgeIm = (edgeIm > 10).*t; %Binarize

%imshow(edgeIm);
%%%%%%%%Part I: Find candidate body parts

%Find candidate torso-head templates
torsos = searchChamferIm(edgeIm,torso_templates,MAX_NUM,.1,0);  
if isempty(torsos), return; end
%Resize to the actual height of the torso
[torsos.len torsos.y] = deal(torsos.len - hWid,torsos.y + hWid);
torsos.type = TOR*ones(size(torsos.resp));

%Find upper legs
ulegs = searchDistIm(edgeIm,S.lens(LUL),S.wids(LUL),MAX_NUM,.09,[-60:15:60]);
if isempty(ulegs) return; end
%Compute a color histogram representation for each segment
uHist = getHistsFast(im,ulegs);
colInds = any(uHist,1); %Sparsify histogram
uHist = uHist(:,colInds);
tt = find(colInds);
[aa,bb,cc] = ind2sub([8 8 8],tt(:));
col_weights = exp(-self_distance([aa bb cc])/10);
%Compute (a weighted) pairwise distance between each candidate upper leg
uHist = uHist./repmat(sum(uHist,2),1,length(tt));
relUHist = sq_wt_distance(uHist,uHist,col_weights);    
ulegs.type = LUL*ones(size(ulegs.resp));

%Copy the same segments for the lower legs
legs = ulegs;
legs.type = LLL*ones(size(legs.resp));

%Find the arms
uarms = searchDistIm(edgeIm,S.lens(LUA),S.wids(LUA),MAX_NUM,.05);
if isempty(uarms) return; end

%Prune away those horizontal arms
uarms.type = LUA*ones(size(uarms.resp));
uarms = subSegs(uarms,uarms.u ~= 1 & uarms.u ~= -1);
if isempty(uarms) return; end   
arms = uarms;
arms.type = LLA*ones(size(arms.resp));

%Force arms to face left
flipArms = find(arms.u > 0);
[arms.u(flipArms),arms.v(flipArms)] = deal(-arms.u(flipArms),-arms.v(flipArms));

%%%%%%%%%Part II: Given the candidates, do dynamic programming to get the best body configuration
[segs cost] = sample_run_full_person(torsos,arms,uarms,legs,ulegs,relUHist);
segs.type = [TOR LUA LLA LUL RUL LLL RLL]';
segs.cost = cost;
