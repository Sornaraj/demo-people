function [segs,costs] = sample_run_full_personc(torsos,arms,uarms,legs,ulegs,relHist)
%[segs,costs] = sample_run_full_personc(torsos,arms,uarms,legs,ulegs,relHist)

BIG_NUM = 1e10;
imVar = .05;
MAX_RESP = .25;

numUArms = length(uarms.resp);
numArms = length(arms.resp);
numULegs = length(ulegs.resp);
numLegs = length(legs.resp);
numTorsos = length(torsos.resp);


%%%%% Part I: Compute pairwise potentials between connected 
%%%%% body parts (ie, lower-arms & upper-arms, upper-arms & torsos, etc..)

%relX and relY are coordinates wrt TORSO coordinate frame
%ie relY = -tlen is where the head is
[relX, relY] = relAllPos(torsos,uarms,-1);
tlen = torsos.len(1);
twid = torsos.w(1);
relY = abs(relY - -tlen);
%These are all the valid arms
%relArms = abs(relX) < twid & relY > 0 & relY < tlen/2;
relUArms = abs(relX) < twid*1.0 & relY < twid*.5;
relUArms = ~relUArms*BIG_NUM;

%Now do the lower arms
[relX, relY] = relAllPos(uarms,arms,-1);
relY = abs(relY - uarms.len(1));
relArms = abs(relX) < uarms.w(1)*1.5 & relY < uarms.w(1)*1.5;
%Enforce a near 90 degree bend in the arm
relArms = relArms & [-uarms.v uarms.u]*[arms.u arms.v]' >= sqrt(3)/2;
relArms = ~relArms*BIG_NUM;

%Now do the upperlegs
[relX, relY] = relAllPos(torsos,ulegs,-1);
relY = abs(relY - tlen);
%relULegs = abs(relX) < twid & relY < twid/2;
relULegs = abs(relX) < (twid*2) & relY < (twid*1)/2;
relULegs = ~relULegs*BIG_NUM;

%Split into left and right
llegs = find(ulegs.u < -1e-5);
rlegs = find(ulegs.u > 1e-5);
numLLegs = length(llegs);
numRLegs = length(rlegs);

%Now do lower legs
[relX, relY] = relAllPos(ulegs,legs,-1);
relY = abs(relY - ulegs.len(1));
%relLegs = abs(relX) < ulegs.w(1)/2 & abs(relY) < ulegs.w(1)*2;
relLegs = abs(relX) < ulegs.w(1) & abs(relY) < ulegs.w(1)*2;

%Only look for people walking left
relLegs = relLegs & [ulegs.v -ulegs.u]*[legs.u legs.v]' > 0;
relLegs = ~relLegs*BIG_NUM;

%Define singleton potentials
armsCost = (MAX_RESP - min(arms.resp,MAX_RESP))/imVar;
legsCost = (MAX_RESP - min(legs.resp,MAX_RESP))/imVar;
torsosCost = (MAX_RESP - min(torsos.resp,MAX_RESP))/(1*imVar);
%Don't care as much about upper legs
ulegsCost = (MAX_RESP - min(ulegs.resp,MAX_RESP))/(2*imVar);
uarmsCost = (MAX_RESP - min(uarms.resp,MAX_RESP))/imVar;

%Try scaling with the size of the guy; think of the cost as the squared error
armsCost = armsCost*arms.len(1)*arms.w(1)*4;
legsCost = legsCost*legs.len(1)*legs.w(1)*4;
torsosCost = torsosCost*torsos.len(1)*torsos.w(1)*4;
ulegsCost = ulegsCost*ulegs.len(1)*ulegs.w(1)*4;
uarmsCost = uarmsCost*uarms.len(1)*uarms.w(1)*4;


%Merge the singleton potentials into 1 pairwise one
relUArms = relUArms + repmat(torsosCost,1,numUArms);
relArms = relArms + repmat(uarmsCost,1,numArms) + repmat(armsCost',numUArms,1);
relLegs = relLegs + repmat(ulegsCost,1,numLegs) + repmat(legsCost',numULegs,1);

scaleFac = 1000;
%scaleFac = 100;
%scaleFac = 5000;

%Convert into probabiltiies
relUArms = exp(-relUArms/scaleFac); relUArms = relUArms./(sum(relUArms(:)) + eps);
relArms = exp(-relArms/scaleFac); relArms = relArms./(sum(relArms(:)) + eps);
relLegs = exp(-relLegs/scaleFac); relLegs = relLegs./(sum(relLegs(:)) + eps);
relULegs = exp(-relULegs/scaleFac); relULegs = relULegs./sum(relULegs(:) + eps);



%%%%%%% PART II: Given the computed pairwise and single potentials, do message passing along
%%%%%%% along the tree to compute the posterior
mArm = sum(relArms,2);
mLeg = sum(relLegs,2);
relULegs = relULegs.*repmat(mLeg',numTorsos,1);
relULegs = relULegs./sum(relULegs(:) + eps);
mLLeg = sum(relULegs(:,llegs),2);
mRLeg = sum(relULegs(:,rlegs),2);
relUArms = relUArms.*repmat(mArm',numTorsos,1);
relUArms = relUArms./sum(relUArms(:) + eps);
mUArm = sum(relUArms,2);
mTor = mUArm.*mLLeg.*mRLeg;

%%%%%%% PART II: Given the computed posterior, sample from it to get body poses
SAMPLE_NO = 2000;
NUM_TYPES = 7;
finalInds = rand(SAMPLE_NO,NUM_TYPES);
costs = repmat(BIG_NUM,SAMPLE_NO,1);

%Cache end locations of leg candidates
legx = legs.x + legs.u.*legs.len;
legy = legs.y + legs.v.*legs.len;
ulegx = ulegs.x + ulegs.u.*ulegs.len;

IM_FAC = 10;

if ~any(mTor),
    segs = [];
    costs = BIG_NUM;
    return;
end

%As we sample by walking down the tree, we're going to add in global contraints
%(ie, left and right leg must look similar)
for sample_i = 1:SAMPLE_NO
    tmp = mTor;
    inds = find(tmp > 0);
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,1));
    torI = inds(tmp(1));
    %showsegIm(im,torsos,torI);
        
    %Record the x position of the torso
    x = torsos.x(torI);
    w = torsos.w(torI);
    tmp = relULegs(torI,llegs);
    inds = find(tmp > 0);
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,4));
    lulegI = llegs(inds(tmp(1)));
    %showsegIm(im,ulegs,llegI);
        
    tmp = relULegs(torI,rlegs);
    inds = find(tmp > 0);
    %inds = inds(relUHist(lulegI,rlegs(inds)) < .05);
    inds = inds(relHist(lulegI,rlegs(inds)) < .05); %****Put it back in
    inds = inds(ulegx(rlegs(inds)) - ulegx(lulegI) > twid*2);
    if isempty(inds), continue; end
    %tmp(inds) = tmp(inds).*exp(-relUHist(lulegI,inds)*IM_FAC);
    tmp(inds) = tmp(inds).*exp(-relHist(lulegI,inds)*IM_FAC);
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,5));
    rulegI = rlegs(inds(tmp(1)));
    %showsegIm(im,ulegs,rlegI);
    
    %Add in extra potential that legs can't be below torso,
    %and left/right have to be on same sides
    tmp = relLegs(lulegI,:);
    inds = find(tmp > 0);
    inds = inds(x - legx(inds) > w);
    if isempty(inds), continue; end
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,6));
    llegI = inds(tmp(1));
    %showsegIm(im,legs,llegI);

    tmp = relLegs(rulegI,:);
    inds = find(tmp > 0);
    inds = inds(legx(inds) - x > w);
    inds = inds(relHist(llegI,inds) < .1);
    %if ~isempty(inds), keyboard; end
    if isempty(inds), continue; end
    tmp(inds) = tmp(inds).*exp(-relHist(llegI,inds)*IM_FAC);
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,7));
    rlegI = inds(tmp(1));
    %showsegIm(im,legs,rlegI);
              
    tmp = relUArms(torI,:);
    inds = find(tmp > 0);
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,2));
    uarmI = inds(tmp(1));
    
    tmp = relArms(uarmI,:);
    inds = find(tmp > 0);
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,3));
    armI = inds(tmp(1));
     
    finalInds(sample_i,:) = [torI uarmI armI lulegI rulegI llegI rlegI];

    %Record the cost
    costs(sample_i) = torsosCost(torI) + uarmsCost(uarmI) + armsCost(armI) + sum(ulegsCost([lulegI rulegI])) + sum(legsCost([lulegI rulegI])) + IM_FAC*relHist(lulegI,rulegI) + IM_FAC*relHist(llegI,rlegI);
end

%Only return the best guy
[costs,zz] = min(costs);

if costs < BIG_NUM,
  inds = finalInds(zz,:);
  segs = appendSegs(appendSegs(appendSegs(appendSegs(subSegs(torsos,inds(1)),subSegs(uarms,inds(2))),subSegs(arms,inds(3))),subSegs(ulegs,inds(4:5))),subSegs(legs,inds(6:7)));
else
  segs = [];
end



