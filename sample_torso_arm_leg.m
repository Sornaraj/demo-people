function [pts,costs] = sample_torso_arm_leg(torsos,heads,uarms,arms,ulegs,legs)
%[pts,costs] = sample_torso_arm_leg(torsos,heads,uarms,arms,ulegs,legs)
%Will flip the arms & legs

%Does not let lower segments overlap with other ones
OVERLAP = 1; %We'll let the rest potential take care of this
REST = 1;
BIG_NUM = 1e10;
PRUNE_HEAD_ARMS = 1; %Removes arm candidates that intersect with the head (simple occlusion reasoning)

LOG_MD = 5;

%Add a dummy segment to each type
seg = subSegsc(torsos,1);seg.resp = LOG_MD; seg.type = 0;
[seg.x,seg.y,seg.u,seg.v] = deal(0);
heads = appendSegsc(heads,seg);
uarms = appendSegsc(uarms,seg);
arms = appendSegsc(arms,seg);
ulegs = appendSegsc(ulegs,seg);
legs = appendSegsc(legs,seg);

%Flip the upper segments
if ~isempty(uarms),
  uarms2 = uarms; [uarms2.u,uarms2.v] = deal(-uarms2.u,-uarms2.v);
  uarms = appendSegs(uarms,uarms2);
end

if ~isempty(ulegs),
  ulegs2 = ulegs; [ulegs2.u ulegs2.v] = deal(-ulegs2.u,-ulegs2.v);
  ulegs = appendSegs(ulegs,ulegs2);
end

%Add a cost for arms pointing up in the air
arms.resp = arms.resp + max(-arms.v/2,0);

numHeads = length(heads.resp);
numArms = length(arms.resp);
numUArms = length(uarms.resp);
numLegs = length(legs.resp);
numULegs = length(ulegs.resp);
numTorsos = length(torsos.resp);

tlen = torsos.len(1);
twid = torsos.w(1);

%relX and relY are coordinates wrt TORSO coordinate frame
%ie relY = -tlen is where the head is

%upper arms
[relX, relY] = relAllPos(torsos,uarms,-1);
relY = relY - -tlen;
%relUArms = abs(relX) < twid & abs(relY) < twid/2;
relUArms = abs(relX) < twid & abs(relY) < twid;
relUArms = ~relUArms*BIG_NUM;
%Add a rest prior that favors it being to one side or another
theta = acos(min(abs([torsos.u torsos.v]*[uarms.v -uarms.u]'),abs([torsos.u torsos.v]*[-uarms.v uarms.u]')))/5;
if REST,
  %Add in a potential favoring far away arms
  relUArms(relUArms == 0) = theta(relUArms == 0);
end



%arms
[relX, relY] = relAllPos(uarms,arms,-1);
relY = relY - uarms.len(1);
%relArms = abs(relX) < uarms.w(1)*1.5 & abs(relY) < uarms.w(1)*1.5;
%Enlargen up the arm potential bounds cause we're adding a prior
relArms = abs(relX) < uarms.w(1)*1.5 & abs(relY) < uarms.len(1);

if OVERLAP,
  %Make sure the arms don't overlap TOO much
  theta = sq_distance([uarms.u uarms.v],[-arms.u -arms.v]);
  relArms(theta < .1) = 0;
  %Extra stuff to avoid overlap; don't let the top of the uarm &
  %bottom of the larm overlap
  d = sq_distance([uarms.x-uarms.u.*uarms.len uarms.y-uarms.v.*uarms.len],...
      [arms.x+arms.u.*arms.len arms.y+arms.v.*arms.len]);
  relArms(d < arms.len(1).^2) = 0;
end
relArms = ~relArms*BIG_NUM;
%Add in a overlapping potential
theta = abs(acos([uarms.u uarms.v]*[arms.u arms.v]'))/5;
%theta = abs(acos([uarms.u uarms.v]*[arms.u arms.v]'));
%relArms(relArms == 0) = theta(relArms == 0);
if REST,
  %Add in a potential favoring far away arms
  relY = ((relY - uarms.len(1)).^2)/500;
  relArms(relArms == 0) = relY(relArms == 0) + theta(relArms == 0);
end


%upper legs
[relX, relY] = relAllPos(torsos,ulegs,-1);
relY = relY - tlen;
%relULegs = abs(relX) < twid & abs(relY) < twid/2;
relULegs = abs(relX) < (twid*2) & abs(relY) < (twid*4)/2;
%Make sure the legs don't point into torso
theta = [torsos.u torsos.v]*[ulegs.u ulegs.v]';
relULegs(theta <= -.3) = 0;
%Do the theta as a soft potential
theta = abs(acos([torsos.u torsos.v]*[ulegs.u ulegs.v]'))/5;
relULegs = ~relULegs*BIG_NUM;
if REST,
  %Add in a potential favoring far away legs
  relY = (relY - tlen).^2/1000;
  %relULegs(relULegs == 0) = relY(relULegs == 0);
  relULegs(relULegs == 0) = relY(relULegs == 0) + theta(relULegs == 0);
end

%legs
[relX, relY] = relAllPos(ulegs,legs,-1);
relY = relY - ulegs.len(1);
%relLegs = abs(relX) < ulegs.w(1) & abs(relY) < ulegs.w(1);
relLegs = abs(relX) < ulegs.w(1) & abs(relY) < ulegs.w(1)*2;

if OVERLAP,
  %theta = sq_distance([ulegs.u ulegs.v],[-legs.u -legs.v]);
  %relLegs(theta < .1) = 0;
  theta = [ulegs.u ulegs.v]*[legs.u legs.v]';
  relLegs(theta < 0) = 0;
end
relLegs = ~relLegs*BIG_NUM;
%Add in a soft potential
theta = abs(acos([ulegs.u ulegs.v]*[legs.u legs.v]'))/5;
%relLegs(relLegs == 0) = theta(relLegs == 0);
if REST,
  %Add in a potential favoring far away legs
  relY = (relY - ulegs.len(1)).^2/500;
  %relLegs(relLegs == 0) = relY(relLegs == 0);
  relLegs(relLegs == 0) = relY(relLegs == 0) + theta(relLegs == 0);
end


%Do head, ignoring the head orientation
[relX,relY] = relAllPos(torsos,heads,0);
relY = relY - -(tlen + heads.len(1));
relHeads = abs(relX) < twid & abs(relY) < twid;
relHeads = ~relHeads*BIG_NUM;

%Make sure the null segments can't point to any real children
[relArms(end,:),relLegs(end,:)] = deal(BIG_NUM);
%Make the null segments okay for any parent to connect to
[relUArms(:,end),relArms(:,end),relULegs(:,end),relLegs(:,end),relHeads(:,end)] = deal(0);

torsosCost = torsos.resp;
legsCost = legs.resp;
armsCost = arms.resp;
ulegsCost = ulegs.resp;
uarmsCost = uarms.resp;
headCost = heads.resp;

%keyboard;
%Merge the singleton potentials into 1 pairwise one
relArms = relArms + repmat(armsCost',numUArms,1) + repmat(uarmsCost,1,numArms);
relLegs = relLegs + repmat(legsCost',numULegs,1) + repmat(ulegsCost,1,numLegs);
relHeads = relHeads + repmat(torsosCost,1,numHeads) + repmat(headCost',numTorsos,1);

%scaleFac = 100;
%scaleFac = 10;
scaleFac = 1;
%scaleFac = .5;

%Convert to probabilities
relUArms = exp(-relUArms/scaleFac); relUArms = relUArms./(sum(relUArms(:)) + eps);
relArms = exp(-relArms/scaleFac); relArms = relArms./(sum(relArms(:)) + eps);
relLegs = exp(-relLegs/scaleFac); relLegs = relLegs./(sum(relLegs(:)) + eps);
relULegs = exp(-relULegs/scaleFac); relULegs = relULegs./sum(relULegs(:) + eps);
relHeads = exp(-relHeads/scaleFac); relHeads = relHeads./sum(relHeads(:) + eps);

mArm = sum(relArms,2); mArm = mArm./sum(mArm(:)+eps);
mLeg = sum(relLegs,2); mLeg = mLeg./sum(mLeg(:)+eps);
relULegs = relULegs.*repmat(mLeg',numTorsos,1);
relULegs = relULegs./sum(relULegs(:) + eps);
mULeg = sum(relULegs,2); mULeg = mULeg./sum(mULeg(:)+eps);
relUArms = relUArms.*repmat(mArm',numTorsos,1);
relUArms = relUArms./sum(relUArms(:) + eps);
mUArm = sum(relUArms,2); mUArm = mUArm./sum(mUArm(:)+eps) ;
mHead = sum(relHeads,2); mHead = mHead./sum(mHead(:)+eps);
%mTor = mUArm.*mULeg.*mHead;
%mTor = mULeg.*mHead;
%mTor = mTor./sum(mTor(:)+eps);
%Comprimise; assume that .5 the time, the arm is occluded
%and do a "mixture" of messages
%This probably won't be enough... I should boost the score of an occluded torsos%mTor = .5*mULeg.*mHead + .5*mUArm.*mULeg.*mHead;
%mTor = .33*mULeg.*mHead + .66*mUArm.*mULeg.*mHead;
mTor1 = mULeg.*mHead;
mTor1 = mTor1./sum(mTor1(:)+eps);
mTor2 = mULeg.*mHead.*mUArm;
mTor2 = mTor2./sum(mTor2(:)+eps);
mTor = mTor1 + mTor2;
mTor = mTor./sum(mTor(:)+eps);

DEBUG = 0;
%Sample and return segments in segs and cost in cost
%tt_sample_model;
%tt_sample_model_simple;
%Just returns pts & cost

SAMPLE_NO = 1000;
NUM_TYPES = 6;
DEBUG = 0;
%Seed our random number generator
rand('state',sum(100*clock));
finalInds = rand(SAMPLE_NO,NUM_TYPES);

if ~any(mTor),
    pts = [];
    costs = inf;
    return;
end

if PRUNE_HEAD_ARMS,
  %Calculate the end position of the arms
  pos = [arms.x + arms.u.*arms.len arms.y + arms.v.*arms.len];
end

pts = zeros(2,2,NUM_TYPES,SAMPLE_NO);
costs = zeros(SAMPLE_NO,1);
for sample_i = 1:SAMPLE_NO
    tmp = mTor;
    inds = find(tmp > 0);
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,1));
    torI = inds(tmp(1));

    tmp = relULegs(torI,:);
    inds = find(tmp > 0);
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,4));
    lulegI = inds(tmp(1));
    %showsegIm(im,ulegs,lulegI);
    
    tmp = relLegs(lulegI,:);
    inds = find(tmp > 0);
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,5));
    llegI = inds(tmp(1));
    %showsegIm(im,legs,llegI);

    tmp = relUArms(torI,:);
    inds = find(tmp > 0);
    %if isempty(inds), continue; end
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,2));
    uarmI = inds(tmp(1));

    %Find the upper-arm/torso angle
    theta = [torsos.u(torI) torsos.v(torI)]*[uarms.u(uarmI) uarms.v(uarmI)]';

    tmp = relArms(uarmI,:);
    inds = find(tmp > 0);
    if PRUNE_HEAD_ARMS & theta < -.3
      %vec(:,1) is "y", vec(:,2) = "x"
      vec  = pos(inds,:) - repmat([torsos.x(torI) torsos.y(torI)],length(inds),1);
      vec = vec*[torsos.u(torI) torsos.v(torI);torsos.v(torI) -torsos.u(torI)]';
      %[v x] = relAllPos(subSegsc(torsos,subSegs(segs,segs.type == segType+ 4),1);
      inds = inds(~(abs(vec(:,2)) < torsos.len(torI) & vec(:,1) < -torsos.len(torI)));
    end
    if isempty(inds), inds = numArms; end
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,3));
    armI = inds(tmp(1));
    
    tmp = relHeads(torI,:);
    inds = find(tmp > 0);
    %if isempty(inds), continue; end
    tmp = cumsum(tmp(inds)); tmp = tmp/tmp(end);
    tmp = find(tmp >= finalInds(sample_i,6));
    headI = inds(tmp(1));
        
    finalInds(sample_i,:) = [torI uarmI armI lulegI llegI headI];
    legI = llegI;
    ulegI = lulegI;
    pts(:,:,1,sample_i) = [torsos.x(torI) - torsos.u(torI)*torsos.len(torI) torsos.y(torI)-torsos.v(torI)*torsos.len(torI);
		  torsos.x(torI) + torsos.u(torI)*torsos.len(torI) torsos.y(torI)+torsos.v(torI)*torsos.len(torI)];
    pts(:,:,2,sample_i) = [uarms.x(uarmI) - uarms.u(uarmI)*uarms.len(uarmI) uarms.y(uarmI)-uarms.v(uarmI)*uarms.len(uarmI);
		  uarms.x(uarmI) + uarms.u(uarmI)*uarms.len(uarmI) uarms.y(uarmI)+uarms.v(uarmI)*uarms.len(uarmI)];
    pts(:,:,4,sample_i) = [arms.x(armI) - arms.u(armI)*arms.len(armI) arms.y(armI)-arms.v(armI)*arms.len(armI);
		  arms.x(armI) + arms.u(armI)*arms.len(armI) arms.y(armI)+arms.v(armI)*arms.len(armI)];
    pts(:,:,3,sample_i) = [ulegs.x(ulegI) - ulegs.u(ulegI)*ulegs.len(ulegI) ulegs.y(ulegI)-ulegs.v(ulegI)*ulegs.len(ulegI);
		  ulegs.x(ulegI) + ulegs.u(ulegI)*ulegs.len(ulegI) ulegs.y(ulegI)+ulegs.v(ulegI)*ulegs.len(ulegI)];
    pts(:,:,5,sample_i) = [legs.x(legI) - legs.u(legI)*legs.len(legI) legs.y(legI)-legs.v(legI)*legs.len(legI);
		  legs.x(legI) + legs.u(legI)*legs.len(legI) legs.y(legI)+legs.v(legI)*legs.len(legI)];
    pts(:,:,6,sample_i) = [heads.x(headI) - heads.u(headI)*heads.len(headI) heads.y(headI)-heads.v(headI)*heads.len(headI);
		  heads.x(headI) + heads.u(headI)*heads.len(headI) heads.y(headI)+heads.v(headI)*heads.len(headI)];
    %Score the cost
    costs(sample_i) = torsos.resp(torI) + uarms.resp(uarmI) + arms.resp(armI) + ulegs.resp(ulegI) + legs.resp(legI) + heads.resp(headI);
    %if DEBUG,
    %  sample_i
    %  pt = pts(:,:,:,sample_i);
    %  pt = permute(pt,[2 1 3]);
    %  seg = pt2seg(pt(:));
    %  showsegIm(uint8(im),seg);
    %  pause;
    %end
end

%Reshape into standard format
pts = reshape(permute(pts,[2 1 3 4]),2*2*NUM_TYPES,SAMPLE_NO);
