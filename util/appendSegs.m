function segs = appendSegs(segs1,segs2),
%segs = appendSegs(segs1,segs2)
%Returns a segs2 appended on the end of segs1

%if isempty(segs1) | isempty(segs1.resp),
if isempty(segs1)
  segs = segs2;
else
  names = fieldnames(segs1);
  for i = 1:length(names),
    f = names{i};
    eval(['segs.' f ' = [segs1.' f '; segs2.' f '];']);
  end
end
