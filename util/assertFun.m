function Y = assert(tst)

if ~tst,
    error('Assertion failed');
end