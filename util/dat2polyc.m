function Y = dat2polyc(dat)
%Y = dat2poly(dat)

if (0),
    Y = dat;
    return;
end

[n,nn] = size(dat);
switch (nn)
 case 3
  %m = 9;
  %ii = cell(m,1);
  %[ii{1},ii{2},ii{3},ii{4},ii{5},ii{6},ii{7},ii{8},ii{9}]  = deal(1,2,3,[1 2],[2 3],[1 3],[1 1],[2 2],[3 3]);
  ii = {1,2,3,[1 2],[2 3],[1 3],[1 1],[2 2],[3 3]};
  %ii = {1,2,3,[1 2],[2 3],[1 3],[1 1],[2 2],[3 3],[1 2 3]}; 
 case 2
  ii = {1,2,[1 2],[1 1],[2 2]};
end

m = length(ii);
Y = ones(n,m);
for i = 1:m,
    Y(:,i) = prod(double(dat(:,ii{i})),2);
end
