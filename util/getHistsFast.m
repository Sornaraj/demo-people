function imHists = getHistsFast(im,segs,binsRGB)
%hist = getHistsFast(im,segs,[binsRGB])
%Tries to do this very fast
%Assumes segs is ALL the same size (!!!)
%Uses RGB 8 X 8 X 8 histogramming
DEBUG = 0;


assert(all(segs.len == segs.len(1)));

if isempty(segs),
  return;
end
len = segs.len(1)-.5;
wid = segs.w(1)-.5;
num = length(segs.resp);
[imy,imx,dummy] = size(im);

if DEBUG,
  segs = subSegs(segs,1:10);
end


%New tactic: try to construct the indices for each point in our segs
%majVec = [segs.u segs.v]*len;
%minVec = [segs.v -segs.u]*wid;

[LEN,WID] = ndgrid(-len:len,-wid:wid);
[ny,nx] = size(LEN);
XI = zeros(num,ny,nx);
YI = zeros(num,ny,nx);
segsx = repmat(segs.x,[1 ny nx]);
segsy = repmat(segs.y,[1 ny nx]);
segsu = repmat(segs.u,[1 ny nx]);
segsv = repmat(segs.v,[1 ny nx]);
LEN = repmat(permute(LEN,[3 1 2]),[num 1 1]);
WID = repmat(permute(WID,[3 1 2]),[num 1 1]);
YI = segsy + segsv.*LEN - segsu.*WID;
XI = segsx + segsu.*LEN + segsv.*WID;
XI = reshape(XI,num,ny*nx);
YI = reshape(YI,num,ny*nx);
XI = min(max(XI,1),imx);
YI = min(max(YI,1),imy);
inds = (round(XI)-1)*imy + round(YI);

%Bin our image
dim = 8;
n = 1/(2*dim):1/dim:1-1/(2*dim);
[R,G,B] = ndgrid(n,n,n);
cmap = [R(:) G(:) B(:)];
tmp = rgb2ind(im,cmap,'nodither');
colInds = double(unique(tmp(:)));
ncol = length(colInds);
imFeats = tmp(inds);

imHists = zeros(num,dim^3);
for i = 1:ncol
  %i
  imHists(:,colInds(i)+1) = sum(imFeats == colInds(i),2);
end









