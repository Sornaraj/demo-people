function Y = isInsideRect(c1,c2,c3,c4,X)
%Y = isInsideRect(c1,c2,c3,c4,X)
%Returns a 1 if X(i) is inside rectange specified by the 4 corners, clockwise

IMAGE = 1;

%pts = cat(3,c1,c2,c3,c4,c1);
%vec = 
v(1,:) = c2 - c1;
v(2,:) = c3 - c2;
v(3,:) = c4 - c3;
v(4,:) = c1 - c4;

%Figure out normals and intercept
if IMAGE,
  n = makeNormal([v(:,2) -v(:,1)],2);
else
  n = makeNormal([-v(:,2) v(:,1)],2);
end

c = [c1; c2; c3; c4];
b = dot(c,n,2);

numX = size(X,1);
Y = zeros(numX,1);
for i = 1:4,
  Y = Y | dot(repmat(n(i,:),numX,1),X,2) - repmat(b(i),numX,1) > 0;
end

Y = ~Y;


