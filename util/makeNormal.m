function y = makeNormal(x,dim)
%y = normal(x,dim)
%Function returns the normalized version of a set of vectors,
%taking the magnitude along dimension dim
if (nargin < 2),
  %Bring first non-singleton dimension to the front
  x = shiftdim(x);
  dim = 1;
end
%Add eps to avoid divide by zero error
magX = max(normVec(x,dim),eps);
siz = size(x,dim);
dimvec = ones(1,length(size(x)));
dimvec(dim) = siz;
y = double(x)./repmat(magX,dimvec);

