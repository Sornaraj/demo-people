function [currVec, lk] = meanshiftInit(X,rad,currVec)
%[Y lk] = meanshiftInit(X,rad,initVec)
%Requires columnwise data to go faster
%Returns the likelihood, or the number of points we're below

[d,n] = size(X);
oldVec = zeros(d,1);
while norm(currVec - oldVec) > 1e-5,
  inds = find(sum((X - repmat(currVec,1,n)).^2) < rad);
  oldVec = currVec;
  currVec = mean(X(:,inds),2);
end
lk = length(inds);
