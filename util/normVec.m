function Y = normVec(X,dim)
%Y = normVec(X,dim)
%Calculates norm of each column of X
if (nargin < 2),
  dim = 1;
end
Y = sqrt(sum(double(X).^2,dim));
