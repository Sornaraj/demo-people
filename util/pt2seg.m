function segs = pt2seg(pts)
%segs = pt2seg(pts)
%Requires columnwise order

[d,numSegs] = size(pts);
numSegTypes = d/4;
%assert(numSegTypes == round(numSegTypes));
%Reshape into (XY) x (top-bottom) x numSegTypes x numSegs
pts = permute(reshape(pts,[2 2 numSegTypes numSegs]),[3 1 2 4]);
cen = permute(.5*pts(:,:,1,:) + .5*pts(:,:,2,:),[1 4 2 3]);
tmp = reshape(squeeze(pts(:,:,2,:) - pts(:,:,1,:)),[numSegTypes 2 numSegs]);
len = reshape(squeeze(sqrt(sum(tmp.^2,2))),[numSegTypes numSegs]);
len = max(len,eps);
u = reshape(squeeze(tmp(:,1,:)),numSegTypes,numSegs)./len;
v = reshape(squeeze(tmp(:,2,:)),numSegTypes,numSegs)./len;

		    
if numSegs > 1,
  segs.x = cen(:,:,1)';
  segs.y = cen(:,:,2)';
  segs.u = u';
  segs.v = v';
  segs.len = len'/2;
  segs.w = segs.len/3;  
else
  segs.x = cen(:,:,1);
  segs.y = cen(:,:,2);
  segs.u = u;
  segs.v = v;
  segs.len = len/2;
  segs.w = segs.len/3;
end

