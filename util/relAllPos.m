function [minDist, majDist] = relAllPos(segs1,segs2,endFlag)
%[xPos yPos] = relAllPos(segs1,segs2,endFlag)
%Function gets the relative position of the upper and lower endpoints of
%segs2 wrt segs1
%[x y], where y "down" the torso, x is "to the right"
%flag = [-1 0 1] corresponds to [upper center lower] segment
%flag defaults to 0
%xPos will be num1 X num2
%If segs1 is empty, we'll just return segs2 wrt to global position
if isempty(segs2),
  minDist = [];
  majDist = [];
  return;
end


num2 = length(segs2.x);

if nargin < 3,
  endFlag = 0;
end

posx = segs2.x;
posy = segs2.y;
switch endFlag,
 case -1,
  posx = posx - segs2.u.*segs2.len;
  posy = posy - segs2.v.*segs2.len;
 case 1,
  posx = posx + segs2.u.*segs2.len;
  posy = posy + segs2.v.*segs2.len;
end

if isempty(segs1),
  minDist = posx';
  majDist = posy';
  return;
end

num1 = length(segs1.x);
%Create num1 X num2 adacency matrices
vecx = repmat(posx',num1,1) - repmat(segs1.x,1,num2);
vecy = repmat(posy',num1,1) - repmat(segs1.y,1,num2);
majDist = vecx.*repmat(segs1.u,1,num2) + vecy.*repmat(segs1.v,1,num2);
minDist = vecx.*repmat(segs1.v,1,num2) - vecy.*repmat(segs1.u,1,num2);









