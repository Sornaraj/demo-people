function [res,respOut] = searchChamferImc(im,template,MAX_NUM,thresh,currAngles,wid)
%[res] = searchChamferIm(im,template,MAX_NUM,thresh,currAngles,wid)
%New format just takes the necessary parameters
%Thresh is now a max number of random samples to draw from
%Maxes over the orientations to get the best one
%Assumes im is a sparse edge image where the edges are labelled with an
%orientation

[origY, origX, dummy] = size(im);

if nargin < 5,
  currAngles = 15:15:180;
end

roi = [1 1 ;origY origX];
[imy,imx,dummy] = size(im);

%Shift our currentAngle
currAngles = -90 - currAngles;
numThetas = length(currAngles);

resp  =zeros([imy imx numThetas]);

%Map each value into a range of acceptable ones
I = [1:24]';
angInc = mod([I-1 I I+1 I+11 I+12 I+13]-1,24)+1;

[u,v] = pol2cart(currAngles*pi/180,1);
step = 2*pi/24;
thets = [0:step:2*pi-step];
[binu,binv] = pol2cart(thets,1);
[dummy,I] = max(binu'*u+binv'*v); I = I';
%I = mod(I-19,24)+1;
I = I-19;

currIm = zeros(size(im));


%Estimate the true length and width
len = size(template,1);
if nargin < 6
    wid = size(template,2);
end
segWid = floor(wid/2);
%templateThet = unique(template(template > 0));

for dir = 1:length(currAngles),
  imResp = zeros(imy,imx);
  currAngle = -90 - currAngles(dir);
  %keyboard;
  mask = imrotate(template,currAngle);
  mask(mask >0) = mod(mask(mask > 0) + I(dir)-1,24)+1;
  for i = unique(mask(mask > 0))',  
    currIm = max(segWid - bwdist(ismember(im,angInc(i,:)),'euclidean'),0)./segWid;
    %currIm = max(segWid - bwdistI(ismember(im,angInc(i,:))),0)./segWid;
    %currIm = max(segWid - eucdist2(ismember(im,angInc(i,:))),0)./segWid;
    resp(:,:,dir) = resp(:,:,dir) + filter2(mask == i,currIm,'same');
  end
end

if nargout == 2,
  res = [];
  respOut = squeeze(resp);
  return;
end

%ord = find(respMask);

%Take a maximum across orientations
%keyboard;

[resp,angI] = max(resp,[],3);
%resp = squeeze(resp); angI = squeeze(angI);
%keyboard;

%Also do non-max suppresion in the directions orthogonal to theta
thets = pi/180*currAngles(angI);
resp = nonmax(resp,thets);
%keyboard;

%Have the value be the mean distance to the edge
resp = resp./sum(template(:) > 0);

%Add extra scaling so that good responses ~ .2
resp  = resp/4;

%keyboard;
%If there are any 0 angles, set them to 0
%resp(thets == -pi/2) = 0;

%Set our thershold to 0
resp(resp < thresh) = 0;

if sum(resp(:) > 0) < MAX_NUM,
    ord = find(resp);
else
    ord = find(sampleWithR(resp(:),MAX_NUM));
end
resp = resp(ord);
[resp,I] = sort(resp);
resp = flipud(resp);
ord = ord(flipud(I));

res.resp = resp;

%[ang i j] = ndgrid(pi/180*currAngles, roi(1,1):roi(2,1), roi(1,2):roi(2,2));
[i,j] = ndgrid(roi(1,1):roi(2,1), roi(1,2):roi(2,2));
%ang = ang(ord); 
ang = pi/180*currAngles(angI(ord));
res.u = cos(ang(:)); res.v = sin(ang(:));
res.x = j(ord);
res.y = i(ord);

%Force all guys to point down
upInds = find(res.v < 0);
res.u(upInds) = -res.u(upInds);
res.v(upInds) = -res.v(upInds);  

%Store the "radius" versions of length and width
res.len = len/2*ones(size(resp));
res.w = wid/2*ones(size(resp));

