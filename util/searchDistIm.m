function [res,respOut] = searchDistImc(im,len,wid,MAX_NUM,thresh,currAngles)
%[res] = searchDistIm(im,len,wid,MAX_NUM,thresh,currAngles)
%New format just takes the necessary parameters
%Thresh is now a max number of random samples to draw from
%Maxes over the orientations to get the best one
%Assumes im is a sparse edge image where the edges are labelled with an
%orientation

[origY, origX, dummy] = size(im);

if nargin < 6,
  currAngles = 15:15:180;
end

roi = [1 1 ;origY origX];
%Add the size of our patch to buffer our ROI
roi = round(roi + max([len wid])*[-1 -1;1 1]);
%Make sure ROI does not exceed our image dimensions
roi(1,:) = max([1 1],roi(1,:));
roi(2,:) = min([origY origX],roi(2,:));
im = im(roi(1,1):roi(2,1),roi(1,2):roi(2,2),:);
[imy,imx,dummy] = size(im);

%Shift our currentAngle
currAngles = -90 - currAngles;
numThetas = length(currAngles);

%resp = zeros([numThetas imy imx]);
resp  =zeros([imy imx numThetas]);
%respMask = zeros(size(resp));

segWid = floor(wid/2);
segMask = zeros(len,wid);
segMask(:,1) = 1;
segMask(:,end) = 1;
%Try summing left and right sides independantly
%segMask = zeros(len,wid,2);
%segMask(:,1,1) = 1;
%segMask(:,end,2) = 1;

%Map our angles into the values of our edge image
[u,v] = pol2cart(currAngles*pi/180,1);
step = 2*pi/24;
thets = [0:step:2*pi-step];
[binu,binv] = pol2cart(thets,1);
[dummy,I] = max(binu'*u+binv'*v); I = I';
angInc = mod([I-7 I-6 I-5 I+5 I+6 I+7]-1,24)+1;
%angInc = mod([I-8 I-7 I-6 I-5 I-4 I+4 I+5 I+6 I+7 I+8]-1,24)+1;
%angInc = mod([I-6 I+6]-1,24)+1;

%Threshold the im (we'll have +1 on the diagonals)
%im = max(segWid - im,0)./segWid;
currIm = zeros(size(im));
for dir = 1:length(currAngles),
    %keyboard;
  imResp = zeros(imy,imx);

  %currIm = max(segWid - eucdist2(ismember(im,angInc(dir,:))),0)./segWid;
  currIm = max(segWid - bwdist(ismember(im,angInc(dir,:)),'quasi-euclidean'),0)./segWid;
  %currIm = max(segWid - sqrt(bwdistI(ismember(im,angInc(dir,:)))),0)./segWid;
  %currAngle = dir * dtheta;
  currAngle = -90 - currAngles(dir);
  %currIm = imrotate(im,-currAngle);
  mask = imrotate(segMask,currAngle);
  resp(:,:,dir) = filter2(mask,currIm,'same');
  %resp(:,:,dir) = min(filter2(mask(:,:,1),currIm,'same'),filter2(mask(:,:,2),currIm,'same'));

  %Set the invalid regions to be thresh
  l = round(size(mask,1)/2);
  w = round(size(mask,2)/2);
%  keyboard;
  resp([1:l end-l:end],:,dir) = thresh;
  resp(:,[1:w end-w:end],dir) = thresh;
end

if nargout == 2,
  respOut = permute(resp,[3 1 2]);
end

%ord = find(respMask);

%Take a maximum across orientations
%keyboard;

[resp,angI] = max(resp,[],3);
%resp = squeeze(resp); angI = squeeze(angI);
%keyboard;

%Also do non-max suppresion in the directions orthogonal to theta
thets = pi/180*currAngles(angI);
resp = nonmax(resp,thets);
%keyboard;

%Have the value be the mean distance to the edge
%Here, max possible value is 1
resp = resp./(len*2);

%Add extra scaling
resp  = resp/2;

%*****REMOVE for min-edge stuff******
resp = resp/2;

%keyboard;
%If there are any 0 angles, set them to 0
%resp(thets == -pi/2) = 0;

%Set our thershold to 0
resp(resp < thresh) = 0;
if sum(resp(:) > 0) < MAX_NUM,
    ord = find(resp);
else
    ord = find(sampleWithR(resp(:),MAX_NUM));
end
resp = resp(ord);
[resp,I] = sort(resp);
resp = flipud(resp);
ord = ord(flipud(I));

res.resp = resp;

%[ang i j] = ndgrid(pi/180*currAngles, roi(1,1):roi(2,1), roi(1,2):roi(2,2));
[i,j] = ndgrid(roi(1,1):roi(2,1), roi(1,2):roi(2,2));
%ang = ang(ord); 
ang = pi/180*currAngles(angI(ord));
res.u = cos(ang(:)); res.v = sin(ang(:));
res.x = j(ord);
res.y = i(ord);

%Force all guys to point down
upInds = find(res.v < 0);
res.u(upInds) = -res.u(upInds);
res.v(upInds) = -res.v(upInds);  

%Store the "radius" versions of length and width
res.len = len/2*ones(size(resp));
res.w = wid/2*ones(size(resp));


