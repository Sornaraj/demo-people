function res = searchImCostCircleRandc (im,wid,MAX_NUM,thresh)
%[res] = getSegments (im,frameNum,wid,thresh)
%Searches binary im for patches of size len by wid of white
%Randomized version, thresh is the number of examples to return

SUPPRESS = 0;
SCALE = 1;
%SCALE = 1/3; %When we weight the background flanks by .5
%SCALE = 1/2; 

%We want our final log potentials to be between 0 & 10
%FINAL_SCALE = 1/3;

%Convert to a probability
%thresh = exp(-thresh/SCALE);

%dtheta = 15;
%ddir = 1; %Maybe go back to 2?
%ddir = min(length(currAngles)-1,ddir);
%dpos = 1; %Maybe go back to 2?

[imy,imx,dummy] = size(im);

len = wid;
%[len,wid] = deal(round(len),round(wid));
rr = [1:(4*wid)];
rr = (rr - mean(rr(:))).^2;
[yy,xx] = ndgrid(rr,rr);
rr = yy + xx;
littleVar = .5;
bigVar = 1;
big = exp(-rr./(bigVar*(wid^2))); big = big./sum(big(:));
little = exp(-rr./(littleVar*(wid^2))); little = little./sum(little(:));
imPatch = little - .5*big;
%Scale it so it
imPatch = imPatch/(sum(imPatch(imPatch > 0)));

%imPatch = zeros(size(yy));
%imPatch(rr < (2*wid).^2) = -1;
%imPatch(rr < wid.^2) = 1;

%ww = sum(imPatch(:) == 1)/sum(imPatch(:) == -1)/2;
[mm,nn] = size(imPatch);

%tmp = filter2(imPatch == 1,im == 0);
%resp = tmp + ww*filter2(imPatch == -1,im == 1);
%tmp = filter2(imPatch.*(imPatch > 0),im == 0);
%resp = tmp + filter2(-imPatch.*(imPatch < 0) ,im == 1);
tmp = filter2(imPatch.*(imPatch > 0), 1 - im);
resp = tmp + filter2(-imPatch.*(imPatch < 0) ,im);



resp = exp(-resp/.5);
resp(tmp == sum(imPatch(imPatch > 0))) = 0;
%resp(tmp == sum(imPatch(:) == 1)) = 0;

resp(resp < thresh) = 0;
%Handle border stuff
wwid = ceil(mm/2);
resp([1:wwid end-wwid:end],:) = 0;
resp(:,[1:wwid end-wwid:end]) = 0;


%ord = find(sampleWithR(exp(-resp(:)/(len*wid)),thresh));
ord = find(sampleWithR(resp(:),MAX_NUM));
resp = resp(ord);
[resp,I] = sort(resp);
resp = flipud(resp);
ord = ord(flipud(I));
%ord = ord(I);
res.resp = resp;
%res.resp = resp.^(1/FINAL_SCALE);

[i j] = ndgrid(1:imy,1:imx);
res.resp = resp;
res.u = zeros(size(resp)); res.v = ones(size(resp));
res.x = j(ord);
res.y = i(ord);
res.len = len/2*ones(size(resp));
res.w = wid/2*ones(size(resp));
%res.fr = frameNum*ones(size(resp));

%Force all guys to point down
upInds = find(res.v < 0);
res.u(upInds) = -res.u(upInds);
res.v(upInds) = -res.v(upInds);
%res.fr = frameNum*ones(size(resp));







