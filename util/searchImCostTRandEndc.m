function res = searchImCostTRandEndc (im,len,wid,MAX_NUM,thresh,currAngles)
%[res,respOut] = getSegments (im,frameNum,len,wid,thresh,currAngles)
%Searches binary im for patches of size len by wid of white
%Randomized version, thresh is the number of examples to return

SUPPRESS = 0;
SCALE = 1/3; %When we weight the background flanks by .5
%SCALE = 1/2; 

%We want our final log potentials to be between 0 & 10
%FINAL_SCALE = 1/3;

[origY, origX, dummy] = size(im);


if ~exist('currAngles'),
  currAngles = 15:15:360;
  %currAngles = 15:15:180;
end
if ~exist('roi'),
  roi = [1 1;origY origX];
end

%Thresh is the number of pixels we're allowing to be misclassified
if ~exist('thresh') | isempty(thresh),
    %thresh = 1*len*wid;
    thresh = 0;
end

%Convert to a probability
%thresh = exp(-thresh/SCALE);

%Add the size of our patch to buffer our ROI
roi = round(roi + max([len wid])*[-1 -1;1 1]);

%Make sure ROI does not exceed our image dimensions
roi(1,:) = max([1 1],roi(1,:));
roi(2,:) = min([origY origX],roi(2,:));

%dtheta = 15;
ddir = 1; %Maybe go back to 2?
ddir = min(length(currAngles)-1,ddir);
dpos = 1; %Maybe go back to 2?

im = im(roi(1,1):roi(2,1),roi(1,2):roi(2,2),:);
[imy,imx,dummy] = size(im);

%Shift our currentAngle
currAngles = -90 - currAngles;

[len,wid] = deal(round(len),round(wid));

%im = uint8(im);
%wwid = ceil(wid/8);
%wwid = 2;

%imPatch = [zeros(len,wid) ones(len,wid) zeros(len,wid)];
%Use felzenswchab style likelihood
%imPatch = [zeros(wid,wid*3); -ones(len,wid) ones(len,wid) -ones(len,wid); -ones(wid,wid*3)];

yy = [-(len-1)/2:(len-1)/2].^2/(.4*len.^2);
xx = [-(wid-1)/2:(wid-1)/2].^2/(.4*wid.^2);
[yy,xx] = ndgrid(yy,xx);
kk = exp(-(yy + xx));
kk = kk.*(len*wid/(sum(kk(:))));
imPatch = [zeros(wid,wid*3); -ones(len,wid) kk -ones(len,wid); -ones(wid,wid*3)];

%yind = 1:len; yind = yind - mean(yind);
%xind = 1:wid; xind = xind - mean(xind);
%[Y,X] = ndgrid(yind/(5*len),xind/(5*wid));
%imPatch = exp(-Y.^2 + -X.^2);

resp = zeros([length(currAngles) imy imx]);
for dir = 1:length(currAngles),
  currAngle = -90 - currAngles(dir);
  kernal = imrotate(imPatch,currAngle);

  %kernal = kernal./sum(kernal(:));  
  %kernal = kernal./sum(kernal(kernal > 0));

  [mm,nn,dummy] = size(kernal);
  
  %currResp = filter2(kernal == 1,im == 0,'valid') + .5*filter2(kernal == 0,im == 1,'valid');
  %tmp = filter2(kernal == 1,im == 0,'valid');
  tmp = filter2(kernal.*(kernal > 0),im == 0,'valid');
  currResp = tmp + .25*filter2(kernal < 0,im == 1,'valid');
  indy = ceil(mm/2):imy-floor(mm/2);
  indx = ceil(nn/2):imx-floor(nn/2);
  if ~isempty(currResp),
    %resp(dir,indy,indx) = exp(-currResp/SCALE);  
    %currResp = exp(-currResp/SCALE);
    currResp = exp(-currResp/(len*wid*SCALE));
    %currResp(tmp == sum(kernal(:) == 1)) = 0;
    currResp(tmp == sum(kernal(kernal > 0))) = 0; 
    resp(dir,indy,indx) = currResp;
  end
end

%keyboard;
resp(resp < thresh) = 0;
%ord = find(sampleWithR(exp(-resp(:)/(len*wid)),thresh));
ord = find(sampleWithR(resp(:),MAX_NUM));
resp = resp(ord);
[resp,I] = sort(resp);
resp = flipud(resp);
ord = ord(flipud(I));
%ord = ord(I);

res.resp = resp;
%res.resp = resp.^(1/FINAL_SCALE);

[ang i j] = ndgrid(pi/180*currAngles,roi(1,1):roi(2,1), roi(1,2):roi(2,2));

res.resp = resp;
ang = ang(ord); res.u = cos(ang); res.v = sin(ang);
res.x = j(ord);
res.y = i(ord);
res.len = len/2*ones(size(resp));
res.w = wid/2*ones(size(resp));
%res.fr = frameNum*ones(size(resp));

%Force all guys to point down

%upInds = find(res.v < 0);
%res.u(upInds) = -res.u(upInds);
%res.v(upInds) = -res.v(upInds);


%res.fr = frameNum*ones(size(resp));
