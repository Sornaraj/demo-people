function D = self_distance(X)
%Dist = self_distance(X)
%X is N x D, and Dist is N x N
N = size(X,1);
D = max(sq_distance(X),0).*(1-eye(N,N));

