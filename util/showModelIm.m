function maskOut=showModelIm(siz,segs)
%mask=showModelIm(size(im),segs)
%Plots the segments whose response is above
%the given threshold
%If opt is selected, we'll show entire image

if isempty(segs),
    maskOut = zeros(siz(1:2));
    return;
end 
%Figure out the corners of our segments
c1 = [segs.x segs.y] - [segs.u segs.v].*repmat(segs.len,1,2) - [segs.v -segs.u].*repmat(segs.w,1,2);
c2 = [segs.x segs.y] - [segs.u segs.v].*repmat(segs.len,1,2) + [segs.v -segs.u].*repmat(segs.w,1,2);
c3 = [segs.x segs.y] + [segs.u segs.v].*repmat(segs.len,1,2) + [segs.v -segs.u].*repmat(segs.w,1,2);
c4 = [segs.x segs.y] + [segs.u segs.v].*repmat(segs.len,1,2) - [segs.v -segs.u].*repmat(segs.w,1,2);

pts = cat(3,c1,c2,c3,c4,c1);
Xpts = squeeze(pts(:,1,:))';
Ypts = squeeze(pts(:,2,:))';

%For each rectangle, see if we are inside it or not
numSegs = length(segs.x);
[yIm,xIm] = deal(siz(1),siz(2));
%xRange = 1:xIm;
%yRange = 1:yIm;
[x1,x2,y1,y2] = deal(max(floor(min(Xpts(:))),1),min(ceil(max(Xpts(:))),xIm),...
		     max(floor(min(Ypts(:))),1),min(ceil(max(Ypts(:))),yIm));

[XX,YY] = ndgrid(x1:x2,y1:y2);
siz = size(XX);
inds = reshape(cat(3,XX,YY),[prod(siz) 2]);
Y = zeros(prod(siz),1);
for i = 1:numSegs
  Y = Y | isInsideRect(c1(i,:),c2(i,:),c3(i,:),c4(i,:),inds);
end
mask = zeros(siz);
mask(Y) = 1;
maskOut = zeros(yIm,xIm);
maskOut(y1:y2,x1:x2) = mask';


