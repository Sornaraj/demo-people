function yOut=showModelPts(siz,pts)
%mask=showModelImMult(size(im),pts)

%We know we're always dealing with rectangles
%Quantize our rectangles to 12 orientations and handle each separately
numTheta = 24;
thetas = [0:numTheta-1]'/numTheta*2*pi + pi/2;
ru = cos(thetas);
rv = -sin(thetas);
phis = (thetas-pi/2)*180/pi;

[d,numSegs] = size(pts);
numSegTypes = d/4;
assert(numSegTypes == round(numSegTypes));
pts = permute(reshape(pts,[2 2 numSegTypes numSegs]),[3 1 2 4]);
segcen = permute(.5*pts(:,:,1,:) + .5*pts(:,:,2,:),[1 4 2 3]);
%tmp = squeeze(pts(:,:,2,:) - pts(:,:,1,:));
tmp = permute(pts(:,:,2,:) - pts(:,:,1,:),[1 2 4 3]);
len = squeeze(sqrt(sum(tmp.^2,2))) + eps;
u = squeeze(tmp(:,1,:))./len;
v = squeeze(tmp(:,2,:))./len;
segcen = reshape(segcen,size(segcen,1)*size(segcen,2),2);
len = len(:)/2;
u = u(:);
v = v(:);
w = len/3;


[dummy,I] = max(abs([ru rv]*[u v]'),[],1);
%[dummy,I] = max([ru rv]*[u v]',[],1);

siz = siz(1:2);
yOut = zeros(siz);

%Represent segments with respect to the center of the image (so they are easier to rotate)
%imcen = (siz(1:2) - 1)/2;
%segs.x = segs.x - imcen(2);
%segs.y = segs.y - imcen(1);

%segs.len(:) = 0;
%segs.w(:) = 0;

%Do each one in turn
for i = 1:numTheta,
  inds = find(I == i);
  if ~isempty(inds),
    coor = segcen(inds,:);
    %Center (wrt to bounding box) & rotate
    maxlen = max(len(inds))*sqrt(2);
    maxCoor = max(coor,[],1) + maxlen;
    minCoor = min(coor,[],1) - maxlen;
    
    %TODO: Rotate the bounding box to get the actual x1x2,y1y2 estimate
    cen = .5*maxCoor + .5*minCoor;
    coor = coor - repmat(cen,length(inds),1);
    phi = phis(i)*pi/180;
    coor = coor*[cos(phi) -sin(phi);sin(phi) cos(phi)]';
    x1 = coor(:,1) - w(inds);
    x2 = coor(:,1) + w(inds);
    y1 = coor(:,2) - len(inds);
    y2 = coor(:,2) + len(inds);
    %sizCoor = round(maxCoor - minCoor + 1);
    %minCoor = minCoor - cen;
    %minx = minCoor(1) - cen(1) - 1;
    %miny = minCoor(2) - cen(2) - 1;
    %minx = min(x1) - 1; miny = min(y1) - 1;
    %dx = 
    %Create a new bounding box with correct center
    dx = max(abs(min(x1)),max(x2));
    dy = max(abs(min(y1)),max(y2));
    mask = zeros(round(2*dy+1),round(2*dx+1));
    dx = dx + 1; dy = dy + 1;
    x1 = round(x1 + dx); x2 = round(x2 + dx);
    y1 = round(y1 + dy); y2 = round(y2 + dy);
    for j = 1:length(inds),
      mask(y1(j):y2(j),x1(j):x2(j)) = mask(y1(j):y2(j),x1(j):x2(j)) + 1;
    end
    %Rotate the mask
    mask = imrotate(mask,phis(i));
    dy = (size(mask,1)-1)/2;
    dx = (size(mask,2)-1)/2;
    yy = round(cen(2)+[-dy:dy]);
    xx = round(cen(1)+[-dx:dx]);
  
    %Bound to be within the right size
    xI = find(xx >=1 & xx <= siz(2));
    yI = find(yy >=1 & yy <= siz(1));
    yOut(yy(yI),xx(xI)) = yOut(yy(yI),xx(xI)) + mask(yI,xI);
  end
end
