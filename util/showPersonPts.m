function colmask = showPersonPts(siz,pts)
%showFullPersonPts(siz,pts)

numTypes = size(pts,1)/4;
switch numTypes
    case 1
        rgbs = 1;
    case 6
        rgbs = [1 2 2 3 3 3];
    case 7
        rgbs = [1 2 3 2 3 2 3];
    case 9
        rgbs = [1 2 3 2 3 2 3 2 3];
    case 10
        rgbs = [1 2 3 2 3 2 3 2 3 3];
end
ptTypes = repmat([1:numTypes],4,1);
  
colmask = zeros([siz(1:2) 3]);
if ~isempty(pts),
  for j = 1:numTypes,
    colmask(:,:,rgbs(j)) = colmask(:,:,rgbs(j)) + showModelPts(siz,pts(ptTypes == j,:));
  end
end
colmask = uint8(colmask*255*2/(size(pts,2)));
%colmask = uint8(colmask*255/800);
imshow(colmask);