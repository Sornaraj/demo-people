function image=showsegIm(im,segs)
%ct=showsegIm(im,segs)

LIN_WID = 4;

%imshow(im);
axis equal;

if ~isfield(segs,'type'),
    segs.type = repmat(1,size(segs.x));
end

if isempty(segs),
  return;
end
    
%Use a dummy color scheme
cmap = [
	1 0 0; %torso=red
	0 1 0; %lua
	0 1 0; %rua-
	1 0 1; %lul
	1 0 1; %rul
	1 1 0; %lla
	1 1 0; %rla
	0 0 1; %lll
	0 0 1; %rll
	0 1 1]; %hed = red	

%Figure out the corners of our segments
c1 = [segs.x segs.y] - [segs.u segs.v].*repmat(segs.len,1,2) - [segs.v -segs.u].*repmat(segs.w,1,2);
c2 = [segs.x segs.y] - [segs.u segs.v].*repmat(segs.len,1,2) + [segs.v -segs.u].*repmat(segs.w,1,2);
c3 = [segs.x segs.y] + [segs.u segs.v].*repmat(segs.len,1,2) + [segs.v -segs.u].*repmat(segs.w,1,2);
c4 = [segs.x segs.y] + [segs.u segs.v].*repmat(segs.len,1,2) - [segs.v -segs.u].*repmat(segs.w,1,2);

  pts = cat(3,c1,c2,c3,c4,c1);
  X = squeeze(pts(:,1,:))';
  Y = squeeze(pts(:,2,:))';
  if size(X,1) == 1,
    X = X';
    Y = Y';
  end
  
  M = zeros(4,4);  
  shapeInserter = vision.ShapeInserter('Shape', 'Lines', 'BorderColorSource', 'Input port');
  
  for i = 1:size(cmap,1),
    inds = find(segs.type == i);
    
    if(~isempty(inds))        
        C = [round(X(:,inds)) round(Y(:,inds))];
        
        for j = 1:size(C,1)-1
            M(j,1) = C(j,1);
            M(j,2) = C(j,2);
            M(j,3) = C(j+1,1);
            M(j,4) = C(j+1,2);
        end
        
        im = step(shapeInserter,im,int32(M),255 .* uint8(cmap(i,:)));
    end
  end
image = im;







