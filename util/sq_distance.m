function d = sq_distance(a,b)
% SQ_DISTANCE - computes squared Euclidean distance matrix
%
% E = sq_distance(A,B)
%
%    A - (MXD) matrix 
%    B - (NXD) matrix
% Returns:
%    E - (MxN) Euclidean distances between vectors in A and B
%
%
% Description : 
%    This fully vectorized (VERY FAST!) m-file computes the 
%    Euclidean distance between two vectors by:
%
%                 ||A-B||^2 = ||A||^2 + ||B||^2 - 2*A.B
%
% B defaults to A

if (nargin < 2)
  b = a;
end

a = double(a);
b = double(b);
m = size(a,1);
n = size(b,1);

if (size(a,2) ~= size(b,2))
   error('A and B should be of same dimensionality');
end

if (m == 1)
  a = [a zeros(size(a,1),1)]; 
  b = [b zeros(size(b,1),1)]; 
end

aa=full(sum(a.*a,2)); bb=full(sum(b.*b,2)); ab=full(a*b'); 
d = repmat(aa,1,n) + repmat(bb',m,1) - 2*ab;

%Imprecision error; force any negative or imaginary elements to 0
%We're not taking square roots, so no need to worry
%d(~isreal(d) | d < 0) = 0;


