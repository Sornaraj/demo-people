function d = sq_wt_distance(X,Y,Asq_g)
%D = sq_wt_distance(X,Y,A)
%X is N by K, Y is M by K, A is K by K
%D is N by M
%dij = (xi - yj)'A(xi-yj)
%Disabled: If A is omitted, we'll use the global Asq_g

%if nargin < 3,
%  global Asq_g;
%end

m = size(X,1);
n = size(Y,1);

aa = full(sum((X*Asq_g).*X,2));
bb = full(sum((Y*Asq_g).*Y,2));
ab = full(X*Asq_g*Y');

d = repmat(aa,1,n) + repmat(bb',m,1) - 2*ab;
