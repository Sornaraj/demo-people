function segs = subSegs(segs,inds),
%segs = subSegs(segs,inds)
%Returns a subset of the segments
if islogical(inds) | issparse(inds),
  inds = find(inds);
end

if ~isempty(inds) & ~isempty(segs),
  names = fieldnames(segs);
  for i = 1:length(names),
    f = names{i};
    eval(['segs.' f ' = segs.' f '(inds,:);'],'');
  end
else
  segs = [];
end





